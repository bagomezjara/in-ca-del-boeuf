# InCA-Delboeuf

## About InCA-Delboeuf
InCA-Delboeuf is a touch-based life enrichment application focused on other animals than humans (OATHs), as well as a research tool for assessing susceptibility against optical ilusions in the context of Animal-Computer Interaction (ACI). It focuses on assessing the susceptibility to the Delboeuf optical ilusion using diferent representatios of values with diferent containers around them. Depending on which representation in selected the values and container sizes are evaluated and analyzed. The susceptibility against the Delboeuf optical ilusion is measured based on the continuous selection of the value in the smaller container. The application presents several representations of values that can be _Control_ representations, change of values but use the same container size, _Experimental_  representations, change of container size but use the same values, and _Free_ representations, which can be a change of container size and of value, the appappearance of this representations are configured in the _Settings_ page.

## How to use the app
1.  This is optional, but you can enter throught your Google account via the Google Login, so the collected data can be asociated to your Google account.
2.  Select the name of the _Instructor_ and the _Subject_ for the experimentation.
3.  The _Instructor_ configures the game parameters on the _Settings_ page. This includes the set of values that can appear, the set of containers sizes, color, number of representations, etc.
4.  The _Instructor_ presents the device to the _Subject_.
5.  The _Subject_ select one of the available game modes.
6.  The _Instructor_ rewards _Subject_ if appropriate.
7.  The results logs can be downloaded at the _Settings_ page in JSON or CSV format.
8.  The results logs can be analyzed at the _Researcher_ page.

##  How to install it

InCA-Delboeuf is a web application developed in sveltekit, so it does not require installation and can be found [here](https://delboeuf.incalab.cl).
On the other hand, if you want to mount your own version on a server you must follow the following steps:

1. Clone this repository.
2. (Optional: for remote data logs): Create a [***Firebase Firestore database***](https://firebase.google.com/):
    - Add ***Google authentication*** to the *firebase* project. 
    - Allow your production domain to authenticator settings in *firebase*.
    - Add ***users*** and ***usageLogs*** collections to *firestore*.
    - Configure *firestore* **rules** to acces *users* and *usageLogs* collections from your deployed app. (Be careful: authenticated users may only have *read* and *write* permisson for the *firestore documents* owned by the user)
    - Create a ***.env*** file in the clonated *InCA-Delboeuf* repository project root and add the *firebase* project *enviroment variables*. An example:
    ```bash
    VITE_APIKEY=...
    VITE_AUTH_DOMAIN=...
    VITE_PROJECT_ID=...
    VITE_STORAGE_BUCKET=...
    VITE_MESSAGE_SENDER_ID=...
    VITE_APP_ID=...
    ```
    - **WARNING**: if the **.env** file is missing, the proyect will not run. In this case, please remove all **Firebase** code of the proyect

3. Install dependencies with  `npm install` (or `pnpm install` or `yarn`)
4. Create a production version with `npm  run  build`.
You can preview the production build with `npm run preview`.
    >  To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Notes for future developers

InCA-Delboeuf is an application that use objects created in the library [InCA-Utils](https://shockerqt.gitlab.io/inca-utils/), but for the creation of the Delboeuf optical ilusion, some changes had to be made, for example 
to make the containers change sizes without afecting the sizes of the values inside, it had to be developed a extra container that it's separated from the main object, in this case, there are two containers, one for the square
container that was default in the base proyect, and also it was added a circular container to create a new shape for the containers trying to emulate the classic Delboeuf ilusion.
The components are container like this:
```bash
    #Container for the circular shape
    .border-container-circle {
        width: var(--container-width);
        height: var(--container-height);
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: var(--inca-tile-background, $inca-tile-background);
        border: var(--border-size,var(--inca-tile-border-size, $inca-tile-border-size)) solid var(--border-color,var(--inca-tile-border-color, $inca-tile-border-color));
        border-radius: 50%;
        cursor:pointer;
    }
    #Container for the square shape separated form the main object
    .border-container-square {
        width: var(--container-width);
        height: var(--container-height);
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: var(--inca-tile-background, $inca-tile-background);
        border: var(--border-size,var(--inca-tile-border-size, $inca-tile-border-size)) solid var(--border-color,var(--inca-tile-border-color, $inca-tile-border-color));
        cursor:pointer;
    }
 ```
This can be useful if someone whats to create a new shape for the containers whitout having the need to change or create a new library. They also react with the colors selected in the ThemePicker.

If someone wanted to add a new mode to the application, this can be achieve by:
1.  Making a new _.svelte_ file in de proyect to create a new figure or numerical representation, this can also be done directly in the code but it's suggested that it's added in a new file.
2.  Import it from the file location, for example:
```bash
import Square from './Square.svelte';
```
3.  Changing the _.json_ files to add the new mode
4.  Adding the new mode to de _Settings.svelte_ file
5.  Make so that the new mode can be called in _MainMenu.svlete_ and in _GameComponent.svelte_. for example:
```bash
{if $config.gameSettings.currentMode == "square"}
    <Square
        on:click={() => select(choices[i], r[i])}
    />
{else}
```
With this, someone who want to change or use this proyect as a base for new work, can have a base idea of how things can be done or change to make new objects or features.

## Related pages

-   Find the developer deployed site [here](https://delboeuf.incalab.cl).
-   Find the open-source code [here](https://gitlab.com/bagomezjara/in-ca-del-boeuf).
-   Find other InCA apps [here](https://buho.dcc.uchile.cl/~inca/).

## Credits/Acknowledgments

-   [Bastián Gómez](https://gitlab.com/bagomezjara), CS Engineering in Computer Science student and developer of this app.
-   [Jérémy Barbay](https://barbay.cl/) from [InCA Labs](https://buho.dcc.uchile.cl/~inca/).
-   Fabián Jaña, who provided the [InCA-Utils](https://shockerqt.gitlab.io/inca-utils/) library for in common elements in InCA apps.
-   Cristobal Sepulveda, who developed [InCA-WhichHasMore](https://inca-which-has-more.vercel.app) that was used as a base for this proyect
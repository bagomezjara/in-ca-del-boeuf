// @ts-nocheck

let synth;
let voices;
let pitch;
let rate;
let lang;
let voiceName;
let playSound;


export function startTextToSpeech(syn, voi, ptc, rt, ln, vn, ps){
	synth = syn;
	voices = voi;
	pitch = ptc;
	rate = rt;
	lang = ln;
	voiceName = vn;
	playSound = ps;
}

export function createNewText(text){
	const utterance = new SpeechSynthesisUtterance(text);
	for (const voice of voices) {
		if (voice.lang !== lang) continue;
		utterance.voice = voice;
		if (voice.name == voiceName) break;
	}
	utterance.pitch = pitch;
	utterance.rate = rate;
	return utterance;
}

export function speak(sound, cancels){
	if (!playSound) return;
	if (cancels) synth.cancel();
	synth.speak(sound);
}

import { logger } from "$lib/stores/stores";
import type { Log } from "../../vite-env";
import type { User } from "firebase/auth";

/**
 * Formats and logs an event.
 * 
 * @param user - The user object or null if anonymous.
 * @param teacher - The name of the teacher.
 * @param learner - The name of the learner.
 * @param action - The action performed.
 * @param results - The results of the action.
 * @returns {Promise<void>} - A promise that resolves when the logging is complete.
 */
export async function formatAndLog(user: User | null, teacher: string, learner: string, action: string, results: any) {
    const now = new Date();
    let eventFb: Log = {
        timestamp: now,
        user: user ? user.displayName : "Anonymous",
        userId: user ? user.uid : "Anonymous",
        date: now.toISOString().slice(0, 10),  // Extracts the date part in "YYYY-MM-DD" format
        time: now.toISOString().slice(11, 19), // Extracts the time part in "HH:MM:SS" format  
        teacher,
        action,
        subject: learner,
        details: {...results},
    };
    logger.log(learner, eventFb);
}



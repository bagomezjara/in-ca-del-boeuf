import { Parser } from '@json2csv/plainjs';
import type { Logs } from '../../vite-env';
import JSZip from 'jszip';


/**
 * Shuffle an array
 * @param array the array
 * @returns the array shuffled
 */
export const shuffleArray = (array: unknown[]) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};



export const asyncLogsToCsvUrl = async (logObject: Logs) => {
  const parser = new Parser({});
  const zip = new JSZip();
  for (const [subject, logs] of Object.entries(logObject)) {
    const csv = parser.parse(logs);
    zip.file(`${subject}.csv`, new Blob([csv], { type: 'text/csv' }));
  }
  const content = await zip.generateAsync({ type: 'blob' });
  return URL.createObjectURL(content);
};

export const logsToJSONUrl = (logObject: Logs) => {
  return URL.createObjectURL(
    new Blob([JSON.stringify(logObject, null, 2)], {
      type: 'application/json',
    })
  );
};

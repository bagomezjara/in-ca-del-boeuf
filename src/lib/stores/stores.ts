import { writable } from 'svelte/store';
import Delboeuf from './Delboeuf.json'
import type { Mode } from 'inca-utils';
import type { State, View, Config, Logs, Log, User } from '../../vite-env';
import { browser } from '$app/environment';

const saveDelay = 2000;

const createState = () => {
  let ready = false;
  let readyScreen = true;


  const { subscribe, set, update } = writable<State>({
    view: 'menu',
    ready,
    readyScreen,
  });

  return {
    set,
    subscribe,
    switchView: (view: View, params?: { mode: Mode }) =>
      update(value => ({ ...value, view, params: params || value.params })),
  };
};

const createConfig = () => {
  const localStorageKey = 'Delboeuf-config';
  let storedData;
  if (browser) {
    storedData = localStorage.getItem(localStorageKey);
  }
  const parsedData = storedData && JSON.parse(storedData);

  // Determine whether to use stored data or default data
  const data =
  parsedData?.version === Delboeuf.config.version
    ? parsedData
    : Delboeuf.config;

const { subscribe, set, update } = writable<Config>(data);


  let timeout: string | number | NodeJS.Timeout;
  subscribe(data => {
    if (browser) {
      if (timeout) clearTimeout(timeout);
      timeout = setTimeout(() => {
        localStorage.setItem(localStorageKey, JSON.stringify(data));
      }, saveDelay);
    }
  });

  // Function to reset config to JSON file contents
  const resetConfig = () => {
    const defaultConfig = { ...Delboeuf.config }; // Clone to a new object to ensure reactivity
    set(defaultConfig);
    if (browser) {
      localStorage.setItem(localStorageKey, JSON.stringify(defaultConfig));
    }
  };

  return {
    subscribe,
    set,
    update,
    resetConfig, // Expose the reset function
  };
};



const createLogger = () => {
  const localStorageKey = 'Delboeuf-logs';
  let storedData
  if (browser) {
    storedData = localStorage.getItem(localStorageKey);
  }
  const data = (storedData && JSON.parse(storedData)) || {};

  const { subscribe, update } = writable<Logs>(data);

  let timeout: string | number | NodeJS.Timeout;
  subscribe(data => {
    // here we can send logs to an API
    if (browser) {
      if (timeout) clearTimeout(timeout);

      timeout = setTimeout(() => {
        localStorage.setItem(localStorageKey, JSON.stringify(data));
      }, saveDelay);
    }

  });

  return {
    subscribe,
    log(subject: string, data: Log) {
      update(logs => {
        if (!logs[subject]) logs[subject] = [];
        return {
          ...logs,
          [subject]: [...logs[subject], data],
        };
      });
    },
  };
};

const createUser = () => {
  const localStorageKey = 'whichHasMore-user';
  let storedData;
  if (typeof window !== 'undefined') {
    storedData = localStorage.getItem(localStorageKey);
  }
  const localUid = crypto.randomUUID();
  const parsedData = storedData ? JSON.parse(storedData) : {
    displayName: 'Anonymous',
    uid: localUid,
    localUid: localUid,
    isLoggedIn: false
  }; // Default isLoggedIn to false

  const { subscribe, set, update } = writable<User>(parsedData);

  let timeout: string | number | NodeJS.Timeout;
  const saveDelay = 1000; // Example delay for saving to localStorage

  subscribe(data => {
    if (typeof window !== 'undefined') {
      if (timeout) clearTimeout(timeout);
      timeout = setTimeout(() => {
        localStorage.setItem(localStorageKey, JSON.stringify(data));
      }, saveDelay);
    }
  });

  return {
    subscribe,
    set,
    update,
  };
}

export const state = createState();
export const config = createConfig();
export const logger = createLogger();
export const user = createUser();
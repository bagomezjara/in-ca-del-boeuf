import type { Mode } from 'inca-utils';

export interface Config {
  version: string;
  profile: Profile;
  gameSettings: GameSettings;
  visualSettings: VisualSettings;
  tts: TTSConfig;
}

interface Profile {
  learnerName: string;
  instructorName: string;
}

interface GameSettings {
  currentMode: string;
  availableModes: string[];
  menuModes: string[];
  passThreshold: number;
  excelThreshold: number;
  playSound: boolean;
  gameLength: number;
  timedRedirect: boolean;
  newGameTime: number;
  gridSize: number;
  defaultValues: number[];
  nbChoices: number;
  redirectTime: number;
  waitingTime: number;
  gameFeatures: boolean;
  equalValues: string[];
  controlChance: number;
  experimentChance: number;
  freeChance: number;
}

interface VisualSettings {
  spaceBetweenTiles: number;
  autoTileWidth: boolean;
  tileWidth: number;
  fullmult: number[];
  multiplier: number[];
  figure: boolean;
  menuRandomValue: boolean;
  menuRandomModes: boolean;
  menuSetValue: number;
  borderTickness: number;
  backColor: string;
  foreColor: string;
  borderColor: string;
  invisible: string;
}

interface TTSConfig {
  lang: string;
  rate: number;
  pitch: number;
  messages: TTSMessages;
}

export interface User {
  displayName: string;
  email: string;
  photoURL: string;
  uid: string;
  localUid: string;
  isLoggedIn: boolean;
  token: string;
}

interface TTSMessages {
  correct: string;
  incorrect: string;
  noMore: string;
  fail: string;
  pass: string;
  excellent: string;
  ready: string;
}


export type View = 'menu' | 'game' | 'about' | 'settings' | 'login';

export interface State {
  view: View;
  params?: {
    mode: Mode;
  };
  ready: boolean,
  readyScreen: boolean,
}

export type Campaign = {
  [mode in Mode]: {
    level: number;
  };
} & {
  version: string;
};

export interface Log {
  timestamp: string | Date | Timestamp | null;
  user: string | null;
  userId: string | null;
  date: string;
  time: string;
  teacher: string;
  subject: string;
  action: string;
  details: Details;
}

interface Timestamp {
  _methodName: string;
}

interface Details {
  result: boolean;
  //Value that help to check if the subject selects the smallest container in the Experimental excersises
  //experimentalDelboeufIlusion: boolean;
  selection: number;
  selectionC: number;
  testName: string;
  optionsV: OptionsValues;
  optionsC: OptionsContainer;
  numberOfChoices: number;
  valuesSet: string;
  multiplierSet: string;
  side: null | string;  // assuming side can be null or a string value
  ContainerFigure: string;
}

interface OptionsValues {
  c0: number;
  c1: number;
  c2: number;
  c3: number;
  c4: number;
}

interface OptionsContainer {
  v0: number;
  v1: number;
  v2: number;
  v3: number;
  v4: number;
}

export interface Logs {
  [subjectName: string]: Log[];
}


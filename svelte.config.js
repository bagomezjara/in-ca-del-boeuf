//import adapter from '@sveltejs/adapter-vercel';
 
//** @type {import('@sveltejs/kit').Config} */
//const config = {
  //kit: {
    //adapter: adapter({
      //runtime: 'nodejs18.x',
    //}),
  //},
//};
 
//export default config;
import adapter from '@sveltejs/adapter-static';

export default {
    kit: {
        adapter: adapter({
            // default options are shown. On some platforms
            // these options are set automatically — see below
            pages: 'build',
            assets: 'build',
            fallback: undefined,
            precompress: false,
            strict: true
        })
    }
};
